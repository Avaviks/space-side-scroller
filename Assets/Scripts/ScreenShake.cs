﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    // Desired duration of the shake effect
    private float shakeTime = 0f;

    // A measure of magnitude for the shake
    private float shakeMagnitude = 0.1f;

    // The initial position of the GameObject
    Vector3 initialPosition;

    void OnEnable()
    {
        initialPosition = transform.localPosition;
    }

    void Update()
    {
        if (shakeTime > 0)
        {
            transform.localPosition = initialPosition + Random.insideUnitSphere * shakeMagnitude;

            shakeTime -= Time.deltaTime;
        }
        else
        {
            shakeTime = 0f;
            transform.localPosition = initialPosition;
        }
    }

    public void TriggerShake(float shakeDuration = 2.0f)
    {
        shakeTime = shakeDuration;
    }
}
