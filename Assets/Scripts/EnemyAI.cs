﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public bool chaseShip = false;
    public float enemySpeed = 1;
    private GameObject playerShip;
    private float distanceOfRay = 100;
    private float pi = Convert.ToSingle(Math.PI);

    // Start is called before the first frame update 
    void Start()
    {
        playerShip = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (!chaseShip)
        {
            // Detects if the player is somewhere in front of the enemy
            RaycastHit2D hit = Physics2D.Raycast(transform.position + (2 * transform.up), transform.up, distanceOfRay);
            if (hit.collider.CompareTag("Player"))
            {
                chaseShip = true;
            }
        }
        else
        {
            // creates a magnitude of 1 2D vector from the centre of the enemy to the player
            Vector2 enemyDirection = new Vector2(playerShip.transform.position.x - transform.position.x, playerShip.transform.position.y - transform.position.y);
            enemyDirection.Normalize();

            // makes sure the angle arccos produces is correct based on the unit circle
            if (enemyDirection.x < 0)
            {
                transform.eulerAngles = new Vector3(0, 0, Mathf.Acos(Vector2.Dot(enemyDirection, Vector2.up)) * 180 / pi);
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, -Mathf.Acos(Vector2.Dot(enemyDirection, Vector2.up)) * 180 / pi);
            }
            transform.position += (transform.up * enemySpeed * Time.deltaTime);
        }
    }

    // Destroys the enemy if they are shot by a bullet
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bullet"))
        {
            Destroy(gameObject);
        }
    }
}
