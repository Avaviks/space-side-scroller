﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GunControl : MonoBehaviour
{
    public GameObject shipPosition;
    public GameObject projectile;
    public float shotDelay = 1;
    public float recoilForce = 35;
    public int ammoLeft = 3;
    public GameObject[] ammoDisplay = new GameObject[11];
    private float pi = Convert.ToSingle(Math.PI);
    private Vector2 mouse;
    private float shotTimer = 0;
    private GameObject mainCamera;
    private AudioSource gunShot;

    // Start is called before the first frame update 
    void Start()
    {
        gunShot = GetComponent<AudioSource>();
        mainCamera = GameObject.FindWithTag("MainCamera");
        for (int i = ammoDisplay.Length - ammoLeft; i > 0; i--)
        {
            ammoDisplay[i + ammoLeft - 1].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // gets the pixel position of the mouse
        mouse.Set(Input.mousePosition.x, Input.mousePosition.y);

        // converts the pixel position to unity editor position
        mouse = Camera.main.ScreenToWorldPoint(mouse);

        // creates a magnitude of 1 2D vector from the centre of the ship in the direction of the mouse position
        mouse.Set(mouse.x - shipPosition.transform.position.x, mouse.y - shipPosition.transform.position.y);
        mouse.Normalize();

        // makes sure the angle arccos produces is correct based on the unit circle
        if (mouse.x < 0)
        {
            transform.eulerAngles = new Vector3(0, 0, Mathf.Acos(Vector2.Dot(mouse, Vector2.up)) * 180 / pi);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 0, -Mathf.Acos(Vector2.Dot(mouse, Vector2.up)) * 180 / pi);
        }

        // Counts the timer down to zero
        if (shotTimer > 0)
        {
            shotTimer -= Time.deltaTime;
        }

        // Checks if the gun can fire when the fire button is pressed
        if (Input.GetButtonDown("Fire1") && shotTimer <= 0 && ammoLeft > 0)
        {
            switch (UnityEngine.Random.Range(0,3))
            {
                case 0:
                    gunShot.pitch = 1.0f;
                    break;
                case 1:
                    gunShot.pitch = 1.04f;
                    break;
                case 2:
                    gunShot.pitch = 0.96f;
                    break;
            }
            gunShot.Play();
            mainCamera.GetComponent<ScreenShake>().TriggerShake(0.1f);
            Vector3 firePosition = transform.position + 1.1f * new Vector3(mouse.x, mouse.y, 0);
            Instantiate(projectile, firePosition, transform.rotation);
            shotTimer = shotDelay;
            shipPosition.GetComponent<Rigidbody2D>().AddForce(mouse * -recoilForce);
            ammoLeft -= 1;
            ammoDisplay[ammoLeft].SetActive(false);
        }
    }
}
