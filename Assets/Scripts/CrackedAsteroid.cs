﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackedAsteroid : MonoBehaviour
{
    public AudioClip[] soundClip = new AudioClip[3];
    public AudioSource sound;

    void Start()
    {
        sound.clip = soundClip[Random.Range(0, 3)];
    }

    // Destroyed on contact with bullet
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bullet"))
        {
            sound.Play();
            Destroy(gameObject);
        }
    }
}
