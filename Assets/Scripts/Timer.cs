﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerDisplay;
    public float startTime = 90;
    private bool timerActive = false;
    public Text display;
    private int seconds;
    private int minutes;
    public GameObject restartButton;
    public GameObject levelSelectButton;

    // Update is called once per frame
    void Update()
    {
        // timer will not start counting until player provides input
        if (Input.anyKeyDown) 
        {
            timerActive = true;
        }
        if (timerActive)
        {
            startTime -= Time.deltaTime;
        }

        // converts the time left to integers with the decimal part removed (not rounded)
        minutes = (int)(startTime / 60);
        // adjustments made to seconds display to make the timer display more accurate (but not 100% accurate)
        seconds = (int)(startTime + 0.9f - (minutes * 60));
        if (seconds <= 9)
        {
            timerDisplay.text = minutes.ToString() + ":0" + seconds.ToString();
        }
        else
        {
            timerDisplay.text = minutes.ToString() + ":" + seconds.ToString();
        }
        

        // Fails the level when the timer reaches zero
        if (startTime <= 0)
        {
            timerDisplay.text = "0:00";
            display.text = "Level Failed";
            gameObject.SetActive(false);
            restartButton.SetActive(true);
            levelSelectButton.SetActive(true);
        }
    }
}
