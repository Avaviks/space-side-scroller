﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipController : MonoBehaviour
{
    public float thrustStrength = 10;
    public float fuelConsumption = 0.5f;
    public float totalFuel = 100;
    public Slider fuelCounter;
    public Text display;
    public GameObject restartButton;
    public GameObject levelSelectButton;
    public GameObject nextLevelButton;
    public GameObject flameUp;
    public GameObject flameDown;
    public GameObject flameLeft;
    public GameObject flameRight;
    private AudioSource upSound;
    private AudioSource downSound;
    private AudioSource leftSound;
    private AudioSource rightSound;
    private SpriteRenderer upSprite;
    private SpriteRenderer downSprite;
    private SpriteRenderer leftSprite;
    private SpriteRenderer rightSprite;
    public AudioClip[] thrusterClip = new AudioClip[6];

    void Start()
    {
        // gets audio components of each flame game object
        upSound = flameUp.GetComponent<AudioSource>();
        downSound = flameDown.GetComponent<AudioSource>();
        leftSound = flameLeft.GetComponent<AudioSource>();
        rightSound = flameRight.GetComponent<AudioSource>();

        // gets sprite renderer components of each flame game object
        upSprite = flameUp.GetComponent<SpriteRenderer>();
        downSprite = flameDown.GetComponent<SpriteRenderer>();
        leftSprite = flameLeft.GetComponent<SpriteRenderer>();
        rightSprite = flameRight.GetComponent<SpriteRenderer>();

        fuelCounter.value = totalFuel;
    }

    // Update is called once per frame
    void Update()
    {
        upSprite.color = Color.clear;
        downSprite.color = Color.clear;
        leftSprite.color = Color.clear;
        rightSprite.color = Color.clear;

        if (totalFuel > 0)
        {
            // Adds a force to the player ship to the right when the positive horizontal button is pressed
            if (Input.GetAxis("Horizontal") > 0)
            {
                // plays sound upon button press
                if (Input.GetKeyDown("right") || Input.GetKeyDown("d"))
                {
                    leftSound.clip = thrusterClip[Random.Range(0, 6)];
                    leftSound.Play();
                }
                GetComponent<Rigidbody2D>().AddForce(Vector2.right * thrustStrength * Time.deltaTime);
                leftSprite.color = Color.white;
                DepleteFuel();
            }
            else
            {
                // stops sound when button is no longer held
                leftSound.Stop();
            }

            // Adds a force to the player ship to the left when the negative horizontal button is pressed
            if (Input.GetAxis("Horizontal") < 0)
            {
                // plays sound upon button press
                if (Input.GetKeyDown("left") || Input.GetKeyDown("a"))
                {
                    rightSound.clip = thrusterClip[Random.Range(0, 6)];
                    rightSound.Play();
                }
                GetComponent<Rigidbody2D>().AddForce(-Vector2.right * thrustStrength * Time.deltaTime);
                rightSprite.color = Color.white;
                DepleteFuel();
            }
            else
            {
                // stops sound when button is no longer held
                rightSound.Stop();
            }

            // Adds a force to the player ship upwards when the positive vertical button is pressed
            if (Input.GetAxis("Vertical") > 0)
            {
                // plays sound upon button press
                if (Input.GetKeyDown("up") || Input.GetKeyDown("w"))
                {
                    downSound.clip = thrusterClip[Random.Range(0, 6)];
                    downSound.Play();
                }
                GetComponent<Rigidbody2D>().AddForce(Vector2.up * thrustStrength * Time.deltaTime);
                downSprite.color = Color.white;
                DepleteFuel();
            }
            else
            {
                // stops sound when button is no longer held
                downSound.Stop();
            }

            // Adds a force to the player ship downwards when the negative vertical button is pressed
            if (Input.GetAxis("Vertical") < 0)
            {
                // plays sound upon button press
                if (Input.GetKeyDown("down") || Input.GetKeyDown("s"))
                {
                    upSound.clip = thrusterClip[Random.Range(0, 6)];
                    upSound.Play();
                }
                GetComponent<Rigidbody2D>().AddForce(-Vector2.up * thrustStrength * Time.deltaTime);
                upSprite.color = Color.white;
                DepleteFuel();
            }
            else
            {
                // stops sound when button is no longer held
                upSound.Stop();
            }

            fuelCounter.value = totalFuel;
        }
        else if (totalFuel <= 0)
        {
            upSound.Stop();
            downSound.Stop();
            leftSound.Stop();
            rightSound.Stop();
        }
        /*else if (Input.GetAxisDown("Vertical") != 0 || Input.GetAxisDown("Horizontal") != 0)
        {
            upSound.Play();
        }*/
    }

    // Runs when the player moves into this trigger
    void OnTriggerEnter2D(Collider2D other)
    {
        // Checks if the tag of the object that enters is "Finish" or "Enemy" or "Asteroid"
        if (other.CompareTag("Asteroid"))
        {
            display.text = "Level Failed";
            gameObject.SetActive(false);
            restartButton.SetActive(true);
            levelSelectButton.SetActive(true);
        }
        else if (other.CompareTag("Enemy"))
        {
            display.text = "Level Failed";
            gameObject.SetActive(false);
            restartButton.SetActive(true);
            levelSelectButton.SetActive(true);
        }
        else if (other.CompareTag("Finish"))
        {
            display.text = "Level Complete";
            gameObject.SetActive(false);
            nextLevelButton.SetActive(true);
            levelSelectButton.SetActive(true);
            upSound.Stop();
            downSound.Stop();
            leftSound.Stop();
            rightSound.Stop();
        }
    }

    // Reduces fuel by specified amount
    void DepleteFuel()
    {
        /*shipSounds.clip = thrusterClip[Random.Range(0, 6)];
        shipSounds.Play();*/
        totalFuel -= fuelConsumption * Time.deltaTime;
    }
}
