﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuFunctions : MonoBehaviour
{
    private AudioSource buttonSound;

    void Start()
    {
        buttonSound = GetComponent<AudioSource>();
        PlaySound();
    }

    // Resets the current scene 
    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Opens a chosen scene 
    public void OpenScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    // Opens a provided link
    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    void PlaySound()
    {
        switch (Random.Range(0, 5))
        {
            case 0:
                buttonSound.pitch = 1.04f;
                break;
            case 1:
                buttonSound.pitch = 1.02f;
                break;
            case 2:
                buttonSound.pitch = 1.0f;
                break;
            case 3:
                buttonSound.pitch = 0.98f;
                break;
            case 4:
                buttonSound.pitch = 0.96f;
                break;
        }
        buttonSound.Play();
    }

    // Closes the application
    public void Quit()
    {
        PlaySound();
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
