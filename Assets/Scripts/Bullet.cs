﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed = 1;

    // Update is called once per frame
    void Update()
    {
        // Moves the bullet forward
        transform.position += transform.up * Time.deltaTime * bulletSpeed;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // Checks if the tag of the object that enters is "Asteroid"
        if (other.CompareTag("Asteroid"))
        {
            // Destroys the bullet but not the object hit
            Destroy(gameObject);
        }
    }
}
